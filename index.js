const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows access to routes defined within our application
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js")

const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.guoofgf.mongodb.net/E-Commerce?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => 
	console.log('Now connected to the MongoDB Atlas.')
);

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Defines "/users" to be included fo all the user routes defined in the userRoutes file
app.use("/users", userRoutes);
app.use("/products", productRoutes);



app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online at port ${process.env.PORT || 4000}`)
});