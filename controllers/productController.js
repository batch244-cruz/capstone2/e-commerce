const Product = require("../models/Product");


// Create a new product
module.exports.addProduct = (data) => {

    let newProduct = new Product ({
        name: data.product.name,
        description: data.product.description,
        price: data.product.price
    });

    return newProduct.save().then((product, error) => {
        if (error){
            return false
        } else{
            return true;
        }
    })
};


// Retrieve all active products
module.exports.getAllActive = () => {
    return Product.find({isActive:true}).then(result => {
        return result;
    })
};


// Retrieving a single product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
};


// Updating a Product
module.exports.updateProduct = (reqParams, reqBody) => {
    
    return Product.findByIdAndUpdate(reqParams.productId, reqBody).then((product, error) => {

        if (error) {
            return false;

        }else {
            return true;
        }
    })
};


// Archiving a product
module.exports.archiveProduct = (reqParams, reqBody) => {

    return Product.findByIdAndUpdate(reqParams.productId, reqBody).then((product, error) => {

        if (error) {
            return false;
        } else {
            return true;
        }
    })
};


// Get all products
module.exports.getAllProducts = () => {

    return Product.find({}).then(result => {
        return result;
    })
}