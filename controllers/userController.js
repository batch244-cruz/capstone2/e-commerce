const bcrypt = require("bcrypt");
const auth = require("../auth")
const User = require("../models/User");
const Product = require("../models/Product");


// Checks if the email already exists

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// The find method returns a record if a match is found
		if (result.length > 0) {
			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		}
	})
};

// User registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,

		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if (error) {
			return false;

		} else {
			return true;
		}
	})
};


// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {

				return {access : auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// Check out a product
module.exports.checkOut = async (data, reqId) => {

	let productPrice = 0
	
		let isProductUpdated = await Product.findById(data.productId).then(async product => {
	
		product.orders.push({orderId : reqId})

		productPrice = product.price

		await product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	})

	let isUserUpdated = User.findById(reqId).then(async user => {

		user.orders.push({products: [{productName: data.productName,quantity: data.quantity}],totalAmount: data.quantity * productPrice})
		// console.log(user)

		await user.save().then((user, error) => {
			if (error) {
			
				return false;
			} else {
				return true;
			}
		})
	})
		
	if (isUserUpdated && isProductUpdated) {
			return false;
		} else {
			return true;
	}
}

// Retrieve user details

module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {

		result.password = "";

		return result;

	});

};

// Updating user as admin
module.exports.updateUser = (reqParams, reqBody) => {

	let updatedUser = {
		isAdmin: reqBody.isAdmin
	};

	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {

		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

module.exports.addToCart = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({productId : data.productId});

		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	});
}