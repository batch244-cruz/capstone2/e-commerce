const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");


// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller to communicate with our database.
router.post("/checkEmail", (req, res) => {

	// .then method uses the result from the controller function and sends it back to the frontend application via res.send method
	userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
});

// Route for the user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
});


// Route to non-admin User checkout	
router.post("/checkout", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		productId: req.body.productId,
		quantity: req.body.quantity
}

	if (userData.isAdmin) {
		res.send(false)
	} else {
		userController.checkOut(data,userData.id).then(resultFromController => res.send(resultFromController));
	}
	
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

router.put("/:userId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	if (data.isAdmin) {
		userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
})

// Route to enroll a user to a course
router.post("/addToCart", auth.verify, (req, res) => {

	let data = {
		// User ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,

		// To know if the logged in user is an admin or not
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId
	}

	if (data.isAdmin == false) {
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});


module.exports = router;