const express = require("express");
const router = express.Router();
const courseController = require("../controllers/productController.js");
const auth = require("../auth");


// Route for creating a product
router.post("/", auth.verify, (req, res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    console.log(data)
    if (data.isAdmin) {

        courseController.addProduct(data).then(resultFromController => res.send(resultFromController))

    } else {
        res.send(false)
    }
    
});

//Retrieving all Active Products
router.get("/active", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Get all product
router.get("/allProducts", (req, res) => {
    courseController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Retrieveing a single product
router.get("/:productId", (req, res) => {
    console.log(req.params.courseId)
    courseController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Updating a product
router.put("/:productId", auth.verify, (req, res) => {

    const data = auth.decode(req.headers.authorization);

    if (data.isAdmin) {
            courseController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
        } else {
            res.send(false)
        }
});


// Route to archive a product
router.patch("/:productId/archive", auth.verify, (req, res) => {
    courseController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});



module.exports = router; 